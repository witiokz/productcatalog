﻿using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using ProductCatalogApplication.Attributes;

namespace ProductCatalogApplication.Controllers
{
    [Route("api/[controller]")]
    public class ProductImageController : Controller
    {
        private readonly IConfig config;

        public ProductImageController(IConfig config)
        {
            this.config = config;
        }

        [HttpGet]
        public ActionResult Get(string id)
        {
            var filePath = Path.Combine(config.ImagesFolder, id);

            if (!System.IO.File.Exists(filePath))
            {
                filePath = Path.Combine(config.ImagesTempFolder, id);
            }

            if (System.IO.File.Exists(filePath))
            { 
                var fileExtensionContentTypeProvider = new FileExtensionContentTypeProvider();
                fileExtensionContentTypeProvider.TryGetContentType(filePath, out string contentType);

                return PhysicalFile(filePath, contentType);
            }

            return NotFound();
        }

        [FileUploadOperationFilter.SwaggerForm("ImportImage", "Upload image file")]
        [HttpPost]
        public string Post(IFormFile file)
        {
            if (file.Length > 0)
            {
                var fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                var filePath = Path.Combine(config.ImagesTempFolder, fileName);

                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }

                return fileName;
            }

            return string.Empty;
        }
    }
}