﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Services.Dto;
using Services.Interfaces;

namespace ProductCatalogApplication.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        IProductService productService;
        private readonly IConfig config;

        public ProductController(IProductService productService, IConfig config)
        {
            this.productService = productService;

            this.config = config;
        }

        // GET api/product
        [HttpGet]
        public IEnumerable<ProductDto> Get(string searchText = null)
        {
            return string.IsNullOrEmpty(searchText) ? productService.GetAll() : productService.Search(searchText);
        }

        // GET api/product/5
        [HttpGet("{id}")]
        public ProductDto Get(int id)
        {
            return productService.GetById(id);
        }

        // POST api/product
        /// <returns>A newly created Product</returns>
        /// <response code="200">Returns the newly created item</response>
        /// <response code="400">If the item is null</response>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public ActionResult Post([FromBody]ProductDto productDto)
        {
            SetModelState(productService.Add(productDto));

            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ProcessFile(productDto.Photo);

            return Ok();
        }

        // PUT api/product/5
        /// <returns>Edit Product</returns>
        /// <response code="200">Returns the edit item</response>
        /// <response code="400">If the item is null</response>   
        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public ActionResult Put(int id, [FromBody]ProductDto productDto)
        {
            productDto.Id = id;

            SetModelState(productService.Update(productDto));

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ProcessFile(productDto.Photo);

            return Ok();
        }

        // DELETE api/product/5
        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public ActionResult Delete(int id)
        {
            productService.Delete(id);

            return Ok();
        }

        private void SetModelState(IEnumerable<KeyValuePair<string, string>> validationResult)
        {
            foreach (var validationResultItem in validationResult)
            {
                ModelState.AddModelError(validationResultItem.Key, validationResultItem.Value);
            }
        }

        private void ProcessFile(string filePath)
        {
            if(string.IsNullOrEmpty(filePath))
            {
                return;
            }

            var fileName = Path.GetFileName(filePath);
            var source = Path.Combine(config.ImagesTempFolder, fileName);
            var destination = Path.Combine(config.ImagesFolder, fileName);

            if (System.IO.File.Exists(source))
            {
                System.IO.File.Move(source, destination);
            }
        }
    }
}
