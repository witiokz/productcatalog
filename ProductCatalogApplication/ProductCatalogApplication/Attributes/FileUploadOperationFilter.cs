﻿using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProductCatalogApplication.Attributes
{
    public class FileUploadOperationFilter : IOperationFilter
    {
        [AttributeUsage(AttributeTargets.Method)]
        public sealed class SwaggerFormAttribute : Attribute
        {
            public SwaggerFormAttribute(string name, string description)
            {
                Name = name;
                Description = description;
            }
            public string Name { get; private set; }

            public string Description { get; private set; }
        }

        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null) return;

            var result = from a in context.ApiDescription.ParameterDescriptions
                         join b in operation.Parameters.OfType<NonBodyParameter>()
                         on a.Name equals b?.Name
                         where a.ModelMetadata.ModelType == typeof(IFormFile)
                         select b;


            result.ToList().ForEach(x =>
            {
                x.In = "formData";
                x.Description = "Upload file.";
                x.Type = "file";
            });
        }
    }
}
