﻿using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace ProductCatalogApplication
{
    public class Config: IConfig
    {
        private const string imagesTempFolderName = "TempUpload";
        private const string imagesFolderName = "Upload";

        private IHostingEnvironment hostingEnvironment;

        public Config(IHostingEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
        }

        public string ImagesTempFolderName
        {
            get
            {
                return imagesTempFolderName;
            }
        }

        public string ImagesFolderName
        {
            get
            {
                return imagesFolderName;
            }
        }

        public string ImagesTempFolder
        {
            get
            {
                var path = Path.Combine(hostingEnvironment.WebRootPath, ImagesTempFolderName);

                CreateDirectoryIfNotExists(path);

                return path;
            }
        }

        public string ImagesFolder
        {
            get
            {
                var path = Path.Combine(hostingEnvironment.WebRootPath, ImagesFolderName);
                CreateDirectoryIfNotExists(path);

                return path;
            }
        }

        private static void CreateDirectoryIfNotExists(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }
}
