﻿namespace ProductCatalogApplication
{
    public interface IConfig
    {
        string ImagesTempFolderName { get; }

        string ImagesFolderName { get; }

        string ImagesTempFolder { get; }

        string ImagesFolder { get; }
    }
}