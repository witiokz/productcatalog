﻿using ProductCatalogApplication;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.UnitTests
{
    public class ConfigFake : IConfig
    {
        public string ImagesTempFolderName => "ImagesTempFolderName";

        public string ImagesFolderName => "ImagesFolderName";

        public string ImagesTempFolder => "ImagesTempFolder";

        public string ImagesFolder => "ImagesFolder";
    }
}
