﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductCatalogApplication;
using ProductCatalogApplication.Controllers;
using Services.Interfaces;
using System.Linq;

namespace Api.UnitTests
{
    [TestClass]
    public class ProductControllerTest
    {
        ProductController controller;
        readonly IProductService service;
        readonly IConfig config;


        public ProductControllerTest()
        {
            service = new ProductServiceFake();
            config = new ConfigFake();
            controller = new ProductController(service, config);
        }

        [TestMethod]
        public void GetReturnsAllItems()
        {
            var result = controller.Get();

            Assert.AreEqual(3, result.Count());
        }
    }
}
