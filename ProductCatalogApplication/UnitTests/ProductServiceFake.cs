﻿using Services.Dto;
using Services.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Api.UnitTests
{
    public class ProductServiceFake : IProductService
    {
        private readonly List<ProductDto> products;

        public ProductServiceFake()
        {
            products = new List<ProductDto>
            {
                new ProductDto { Id = 1, Name = "Product1", Code = "Code1", Price = 1 },
                new ProductDto { Id = 2, Name = "Product2", Code = "Code2", Price = 2 },
                new ProductDto { Id = 3, Name = "Product3", Code = "Code3", Price = 3 },
            };
        }

        public IEnumerable<KeyValuePair<string, string>> Add(ProductDto product)
        {
            product.Id = products.Count + 1;

            products.Add(product);

            return new List<KeyValuePair<string, string>>();
        }

        public void Delete(int id)
        {
            var existing = products.First(a => a.Id == id);
            products.Remove(existing);
        }

        public IEnumerable<ProductDto> GetAll()
        {
            return products;
        }

        public ProductDto GetById(int id)
        {
            return products.Where(a => a.Id == id)
            .FirstOrDefault();
        }

        public IEnumerable<ProductDto> Search(string searchText)
        {
            return products.Where(i => i.Name == searchText);
        }

        public IEnumerable<KeyValuePair<string, string>> Update(ProductDto product)
        {
            var existing = products.First(a => a.Id == product.Id);

            if(existing != null)
            {
                existing.Name = product.Name;
                existing.Code = product.Code;
                existing.Price = product.Price;
            }

            return new List<KeyValuePair<string, string>>();
        }
    }
}
