﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using Web.ViewModels;
using System.Linq;
using Web.Helpers;

namespace Web.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductAPI.Client productApiClient;
        private const string BaseUrl = "http://localhost:44958/";
        private const string ImagePath = BaseUrl + "/api/productImage?id=";
        private const string ConfirmValue = "Price must be confirmed";

        public ProductController()
        {
            productApiClient = new ProductAPI.Client(BaseUrl);
        }

        // GET: Product
        public async Task<IActionResult> Index(string searchText = null)
        {
            ViewData["SearchText"] = searchText;

            var productList = await productApiClient.ApiProductGetAsync(searchText);
            var productViewModelList = productList.Select(product => Mappers.ProductToProductViewModel(product));

            return View(productViewModelList);
        }

        // GET: Product/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await productApiClient.ApiProductByIdGetAsync(id.Value);

            if (product == null)
            {
                return NotFound();
            }

            var productViewModel = Mappers.ProductToProductViewModel(product);

            return View(productViewModel);
        }

        // GET: Product/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Code,Name,Price,Photo,PhotoUrl,IsPriceConfirmed")] ProductSaveViewModel productSaveViewModel, IFormFile file)
        {
            if (file != null && file.Length > 0)
            {
                var filePath = await productApiClient.ApiProductImagePostAsync(new ProductAPI.FileParameter(file.OpenReadStream(), file.FileName),
                    new CancellationToken());

                productSaveViewModel.Photo = filePath;
                productSaveViewModel.PhotoUrl = ImagePath + filePath;
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var product = Mappers.ProductViewModelToProduct(productSaveViewModel);

                    await productApiClient.ApiProductPostAsync(product);
                    return RedirectToAction(nameof(Index));
                }
                catch (ProductAPI.SwaggerException ex)
                {
                    productSaveViewModel.ProductMustBeConfirmed = ex.Response.Contains(ConfirmValue);

                    SetModelState(ex.Response);
                }
            }
            return View(productSaveViewModel);
        }

        // GET: Product/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await productApiClient.ApiProductByIdGetAsync(id.Value);

            if (product == null)
            {
                return NotFound();
            }

            var productSaveViewModel = Mappers.ProductToProductSaveViewModel(product);
            productSaveViewModel.PhotoUrl = ImagePath + product.Photo;

            return View(productSaveViewModel);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, [Bind("Id,Code,Name,Price,Photo,PhotoUrl,IsPriceConfirmed")] ProductSaveViewModel productSaveViewModel, IFormFile file)
        {
            if (file != null && file.Length > 0)
            {
                var filePath = await productApiClient.ApiProductImagePostAsync(new ProductAPI.FileParameter(file.OpenReadStream(), file.FileName),
                    new CancellationToken());

                productSaveViewModel.Photo = filePath;
                productSaveViewModel.PhotoUrl = ImagePath + filePath;
            }

            if (id != productSaveViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var product = Mappers.ProductViewModelToProduct(productSaveViewModel);

                    await productApiClient.ApiProductByIdPutAsync(id.Value, product);
                    return RedirectToAction(nameof(Index));
                }
                catch (ProductAPI.SwaggerException ex)
                {
                    productSaveViewModel.ProductMustBeConfirmed = ex.Response.Contains(ConfirmValue);

                    SetModelState(ex.Response);
                }
            }
            return View(productSaveViewModel);
        }

        // GET: Product/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await productApiClient.ApiProductByIdGetAsync(id.Value);

            if (product == null)
            {
                return NotFound();
            }

            var productViewModel = Mappers.ProductToProductViewModel(product);

            return View(productViewModel);
        }

        // POST: ProductDtoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            await productApiClient.ApiProductByIdDeleteAsync(id.Value);

            return RedirectToAction(nameof(Index));
        }

        // GET: Product/Export
        public async Task<IActionResult> Export()
        {
            var products = await productApiClient.ApiProductGetAsync(null);

            var index = 1;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Products");

                worksheet.Cells[index, 1].Value = "Code";
                worksheet.Cells[index, 2].Value = "Name";
                worksheet.Cells[index, 3].Value = "Price";
                worksheet.Cells[index, 4].Value = "Last Updated";

                foreach (var product in products)
                {
                    index++;
                    worksheet.Cells[index, 1].Value = product.Code;
                    worksheet.Cells[index, 2].Value = product.Name;
                    worksheet.Cells[index, 3].Value = product.Price;
                    worksheet.Cells[index, 4].Value = product.LastUpdated.HasValue ?
                        product.LastUpdated.Value.ToString("yyyy-MM-dd HH:mm:ss") : "";
                }
                return File(
                    fileContents: package.GetAsByteArray(),
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: string.Format("products-{0}.xlsx", DateTime.Now.Ticks)
                );
            }
        }

        private void SetModelState(string response)
        {
            var responseItems = JObject.Parse(response);

            foreach (var responseItem in responseItems)
            {
                ModelState.AddModelError(responseItem.Key, responseItem.Value[0].ToString());
            }
        }
    }
}
