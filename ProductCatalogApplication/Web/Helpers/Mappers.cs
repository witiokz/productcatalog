﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.ProductAPI;
using Web.ViewModels;

namespace Web.Helpers
{
    public static class Mappers
    {
        public static ProductViewModel ProductToProductViewModel(ProductDto product)
        {
            return new ProductViewModel
            {
                Code = product.Code,
                Name = product.Name,
                Id = (product.Id ?? 0).ToString(),
                Photo = product.Photo,
                Price = product.Price.ToString(),
                LastUpdated = product.LastUpdated.HasValue ? product.LastUpdated.Value.ToString("yyyy-MM-dd HH:mm:ss") : ""
            };
        }

        public static ProductSaveViewModel ProductToProductSaveViewModel(ProductDto product)
        {
            return new ProductSaveViewModel
            {
                Code = product.Code,
                Name = product.Name,
                Id = product.Id ?? 0,
                Photo = product.Photo,
                Price = product.Price
            };
        }

        public static ProductDto ProductViewModelToProduct(ProductSaveViewModel productSaveViewModel)
        {
            var product = new ProductDto
            {
                Code = productSaveViewModel.Code,
                Name = productSaveViewModel.Name,
                Price = productSaveViewModel.Price,
                IsPriceConfirmed = productSaveViewModel.IsPriceConfirmed
            };

            if (!string.IsNullOrEmpty(productSaveViewModel.Photo))
            {
                product.Photo = productSaveViewModel.Photo;
            }

            return product;
        }
    }
}
