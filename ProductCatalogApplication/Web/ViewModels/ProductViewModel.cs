﻿using Web.ProductAPI;

namespace Web.ViewModels
{
    public class ProductViewModel
    {
        public string Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Photo { get; set; }

        public string Price { get; set; }

        public string LastUpdated { get; set; }
    }
}
