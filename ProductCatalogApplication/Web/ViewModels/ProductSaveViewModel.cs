﻿using System.ComponentModel.DataAnnotations;

namespace Web.ViewModels
{
    public class ProductSaveViewModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public string Photo { get; set; }

        public string PhotoUrl { get; set; }

        [Display(Name="Please confirm price is higher than 999")]
        public bool IsPriceConfirmed { get; set; }

        public bool ProductMustBeConfirmed { get; set; }
    }
}
