﻿using Data.Interfaces;
using Services.Dto;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Validation
{
    public class ValidateService: IValidateService
    {
        private readonly IProductRepository productRepository;

        public ValidateService(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public IEnumerable<KeyValuePair<string, string>> GetValidationResult(ProductDto productDto)
        {
            var result = new List<KeyValuePair<string, string>>();

            if (string.IsNullOrEmpty(productDto.Code))
            {
                result.Add(new KeyValuePair<string, string>("Code", "Code is required"));
            }

            if(!string.IsNullOrEmpty(productDto.Code))
            {
                var existingProduct = productRepository.GetByCode(productDto.Code);

                if(existingProduct != null && existingProduct.Id != productDto.Id)
                {
                    result.Add(new KeyValuePair<string, string>("Code", "Code must be unique"));
                }
            }

            if (productDto.Price <= 0)
            {
                result.Add(new KeyValuePair<string, string>("Price", "Price must be higher than 0"));
            }

            if (productDto.Price > 999 && !productDto.IsPriceConfirmed)
            {
                result.Add(new KeyValuePair<string, string>("Price", "Price must be confirmed"));
            }

            return result;
        }
    }
}
