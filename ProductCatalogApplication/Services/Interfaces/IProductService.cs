﻿using Domain;
using Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IProductService
    {
        IEnumerable<ProductDto> GetAll();

        IEnumerable<ProductDto> Search(string searchText);

        ProductDto GetById(int id);

        IEnumerable<KeyValuePair<string, string>> Add(ProductDto product);

        IEnumerable<KeyValuePair<string, string>> Update(ProductDto product);

        void Delete(int id);
    }
}
