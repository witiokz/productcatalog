﻿using Services.Dto;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IValidateService
    {
        IEnumerable<KeyValuePair<string, string>> GetValidationResult(ProductDto productDto);
    }
}
