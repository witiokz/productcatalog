﻿using Data.Interfaces;
using Domain;
using Services.Dto;
using Services.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository productRepository;
        private readonly IValidateService validateService;

        public ProductService(IProductRepository productRepository, IValidateService validateService)
        {
            this.productRepository = productRepository;

            this.validateService = validateService;
        }

        public IEnumerable<ProductDto> GetAll()
        {
            return from product in productRepository.GetAll()
                   select new ProductDto()
                   {
                       Id = product.Id,
                       Name = product.Name,
                       Code = product.Code,
                       Price = product.Price,
                       Photo = product.Photo,
                       LastUpdated = product.LastUpdated
                   };
        }

        public IEnumerable<ProductDto> Search(string searchText)
        {
            return from product in productRepository.Search(searchText)
                   select new ProductDto()
                   {
                       Id = product.Id,
                       Name = product.Name,
                       Code = product.Code,
                       Price = product.Price,
                       Photo = product.Photo,
                       LastUpdated = product.LastUpdated
                   };
        }

        public ProductDto GetById(int id)
        {
            var product = productRepository.GetById(id);

            if (product != null)
            {
                return new ProductDto
                {
                    Id = product.Id,
                    Name = product.Name,
                    Code = product.Code,
                    Price = product.Price,
                    Photo = product.Photo,
                    LastUpdated = product.LastUpdated
                };

            }

            return null;
        }

        public IEnumerable<KeyValuePair<string, string>> Add(ProductDto productDto)
        {
            var validationResult = validateService.GetValidationResult(productDto);

            if (validationResult.Count() > 0)
            {
                return validationResult;
            }

            if (productDto != null)
            {
                var product = new Product
                {
                    Id = productDto.Id,
                    Name = productDto.Name,
                    Code = productDto.Code,
                    Price = productDto.Price,
                    Photo = productDto.Photo
                };

                product.LastUpdated = DateTime.UtcNow;

                productRepository.Add(product);
            }

            return validationResult;
        }

        public IEnumerable<KeyValuePair<string, string>> Update(ProductDto productDto)
        {
            var validationResult = validateService.GetValidationResult(productDto);

            if(validationResult.Count() > 0)
            {
                return validationResult;
            }

            if (productDto != null)
            {
                var product = productRepository.GetById(productDto.Id);

                if(product != null)
                {
                    product.Name = productDto.Name;
                    product.Code = productDto.Code;
                    product.Price = productDto.Price;
                    product.Photo = productDto.Photo;
                    product.LastUpdated = DateTime.UtcNow;

                    productRepository.Update(product);
                }
            }

            return validationResult;
        }

        public void Delete(int id)
        {
            var product = productRepository.GetById(id);

            if (product != null)
            {
                productRepository.Delete(product);
            }
        }
    }
}
